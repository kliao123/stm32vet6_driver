#include "Dr_SPI.h"

/*通讯等待超时时间*/
#define SPI_CHECK_EVENT_TIMEOUT ((uint32_t)0x100000)

/*用于读取时发送的无效数据,这个数据可以是任意值*/
#define DUMMY_BYTE 0xFFu

/*spi 起始和停止信号*/
#define SPI_FLASH_CS_LOW()    GPIO_ResetBits(GPIOC, GPIO_Pin_0)
#define SPI_FLASH_CS_HIGH()   GPIO_SetBits(GPIOC, GPIO_Pin_0)

/*flash的状态寄存器中的busy位，用于检测flash是否已执行完相应的操作*/
#define FLASH_Status_register_busy_flag 0x01u

/*页写入的大小，不得超过256字节*/
#define SPI_FLASH_PerWritePageSize 256u



/* used to debug */
#define FLASH_DEBUG 0

#if FLASH_DEBUG
#define FLASH_DEBUG_INFO printf
#else
#define FLASH_DEBUG_INFO
#endif



static void SPI_GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE);

  /* 配置SPI 的 CS 引脚，普通IO 即可 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; //PC0
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //推挽输出模式
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  
  /* 配置SPI 的 SCK 引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5; //PA5
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用推挽模式
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  /* 配置SPI 的 MISO 引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; //PA6
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  /* 配置SPI 的 MOSI 引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7; //PA7
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* 拉高CS引脚电平,处于停止状态 */
  SPI_FLASH_CS_HIGH();
}


static void SPI_Config(void)
{
  SPI_InitTypeDef SPI_InitTypeStruct;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

  /*SPI_Direction
  本成员设置SPI 的通讯方向，可设置为双线全双工(SPI_Direction_2Lines_FullDuplex)，
  双线只接收(SPI_Direction_2Lines_RxOnly)，单线只接收(SPI_Direction_1Line_Rx)、单线只
  发送模式(SPI_Direction_1Line_Tx)*/
  SPI_InitTypeStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;

  /*SPI_Mode
  本成员设置SPI 工作在主机模式(SPI_Mode_Master)或从机模式(SPI_Mode_Slave )，这
  两个模式的最大区别为SPI 的SCK 信号线的时序，SCK 的时序是由通讯中的主机产生的。
  若被配置为从机模式，STM32 的SPI 外设将接受外来的SCK 信号*/
  SPI_InitTypeStruct.SPI_Mode = SPI_Mode_Master;

  /*SPI_DataSize
  本成员可以选择SPI 通讯的数据帧大小是为8 位(SPI_DataSize_8b)还是16 位
  (SPI_DataSize_16b)*/
  SPI_InitTypeStruct.SPI_DataSize = SPI_DataSize_8b;

  /*SPI_CPOL 和SPI_CPHA
  这两个成员配置SPI 的时钟极性CPOL 和时钟相位CPHA，这两个配置影响到SPI 的通
  讯模式，关于CPOL 和CPHA 的说明参考前面“通讯模式”小节。
  时钟极性CPOL 成员，可设置为高电平(SPI_CPOL_High)或低电平(SPI_CPOL_Low )。
  时钟相位CPHA 则可以设置为SPI_CPHA_1Edge(在SCK 的奇数边沿采集数据) 或
  SPI_CPHA_2Edge (在SCK 的偶数边沿采集数据)*/
  SPI_InitTypeStruct.SPI_CPOL = SPI_CPOL_High;
  SPI_InitTypeStruct.SPI_CPHA = SPI_CPHA_2Edge;

  /*SPI_NSS
  本成员配置NSS 引脚的使用模式，可以选择为硬件模式(SPI_NSS_Hard )与软件模式
  (SPI_NSS_Soft )，在硬件模式中的SPI 片选信号由SPI 硬件自动产生，而软件模式则需要
  我们亲自把相应的GPIO 端口拉高或置低产生非片选和片选信号。实际中软件模式应用比
  较多*/
  SPI_InitTypeStruct.SPI_NSS = SPI_NSS_Soft;

  /*SPI_BaudRatePrescaler
  本成员设置波特率分频因子，分频后的时钟即为SPI 的SCK 信号线的时钟频率。这个
  成员参数可设置为fpclk 的2、4、6、8、16、32、64、128、256 分频*/
  SPI_InitTypeStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;

  /*SPI_FirstBit
  所有串行的通讯协议都会有MSB 先行(高位数据在前)还是LSB 先行(低位数据在前)的
  问题，而STM32 的SPI 模块可以通过这个结构体成员，对这个特性编程控制*/
  SPI_InitTypeStruct.SPI_FirstBit = SPI_FirstBit_MSB;

  /*SPI_CRCPolynomial
  这是SPI 的CRC 校验中的多项式，若我们使用CRC 校验时，就使用这个成员的参数
  (多项式)，来计算CRC 的值*/
  SPI_InitTypeStruct.SPI_CRCPolynomial = 7; //实际上没用到这个CRC功能，所以这个参数无所谓

  SPI_Init(SPI1, &SPI_InitTypeStruct);
  SPI_Cmd(SPI1, ENABLE);
}


void FLASH_Init(void)
{
	SPI_GPIO_Config();
	SPI_Config();
}

static uint8_t SPI_TIMEOUT_UserCallback(uint8_t errorCode)
{
	/* 使用串口 printf 输出错误信息，方便调试 */
	FLASH_DEBUG_INFO("SPI timeout! errorCode = %d\n",errorCode);
	
	return errorCode;
}


static uint8_t SPI_FLASH_Read_and_Write_Byte(uint8_t SendData)
{
		/*设置超时等待时间*/
	uint32_t SPITimeout = SPI_CHECK_EVENT_TIMEOUT;

	/* 等待发送缓冲区为空，TXE 事件 */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
	{
		if ((SPITimeout--) == 0) return SPI_TIMEOUT_UserCallback(1);
	}

	/* 写入数据寄存器，把要写入的数据写入发送缓冲区 */
	SPI_I2S_SendData(SPI1, SendData);

	SPITimeout = SPI_CHECK_EVENT_TIMEOUT;

	/* 等待接收缓冲区非空，RXNE 事件 */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET)
	{
		if ((SPITimeout--) == 0) return SPI_TIMEOUT_UserCallback(2);
	}

	/* 读取数据寄存器，获取接收缓冲区数据 */
	return SPI_I2S_ReceiveData(SPI1);
}



//读取ID号
uint32_t SPI_Read_Flash_ID(void)
{
	uint8_t  IdDataNum;
	uint32_t buffer[3];
	uint32_t Falsh_ID;
	
  /* 拉低CS引脚电平，起始信号 */
  SPI_FLASH_CS_LOW();

	/* send read ID commed (0x9F) */
	SPI_FLASH_Read_and_Write_Byte(W25X_JedecDeviceID);

	for(IdDataNum = 0; IdDataNum < 3u; IdDataNum++)
	{
		buffer[IdDataNum] = (uint32_t)SPI_FLASH_Read_and_Write_Byte(DUMMY_BYTE);
	}
	
  /* 拉高CS引脚电平,停止信号 */
  SPI_FLASH_CS_HIGH();

	Falsh_ID = ((buffer[0] << 16) | (buffer[1] << 8) | buffer[2]);

	return Falsh_ID;
}

static void SPI_FLASH_WaitForWriteEnd(void)
{
	u8 FLASH_Status = 0;

	SPI_FLASH_CS_LOW();

	/* 发送 读状态寄存器 命令 */
	SPI_FLASH_Read_and_Write_Byte(W25X_ReadStatusReg);

	/* 若 FLASH 忙碌，则等待 */
	do
	{
		/* 读取 FLASH 芯片的状态寄存器 */
		FLASH_Status = SPI_FLASH_Read_and_Write_Byte(DUMMY_BYTE);
	}
	while((FLASH_Status & FLASH_Status_register_busy_flag) == FLASH_Status_register_busy_flag); /* 正在写入标志 */

	/* 停止信号 FLASH: CS 高 */
	SPI_FLASH_CS_HIGH();

	//FLASH_DEBUG_INFO("flash not busy\n");
}

/**
* @brief 向 FLASH 发送 写使能 命令
* @param none
* @retval none
*/
static void SPI_FLASH_WriteEnable(void)
{
	/* 通讯开始：CS 低 */
	SPI_FLASH_CS_LOW();

	/* 发送写使能命令*/
	SPI_FLASH_Read_and_Write_Byte(W25X_WriteEnable);

	/*通讯结束：CS 高 */
	SPI_FLASH_CS_HIGH();
}

/**
* @brief 擦除 FLASH 扇区
* @param SectorAddr：要擦除的扇区地址
* @retval 无
*
*调用扇区擦除指令时注意输入的地址要对齐到 4KB
*/
void SPI_FLASH_SectorErase(uint32_t SectorAddr)
{
	/* 发送 FLASH 写使能命令 */
	SPI_FLASH_WriteEnable();
	
	SPI_FLASH_WaitForWriteEnd();
	
	/* 擦除扇区 */
	/* 选择 FLASH: CS 低电平 */
	SPI_FLASH_CS_LOW();
	
	/* 发送扇区擦除指令*/
	SPI_FLASH_Read_and_Write_Byte(W25X_SectorErase);
	
	/*发送擦除扇区地址的高位*/
	SPI_FLASH_Read_and_Write_Byte((SectorAddr & 0xFF0000) >> 16);
	
	/* 发送擦除扇区地址的中位 */
	SPI_FLASH_Read_and_Write_Byte((SectorAddr & 0xFF00) >> 8);
	
	/* 发送擦除扇区地址的低位 */
	SPI_FLASH_Read_and_Write_Byte(SectorAddr & 0xFF);
	
	/* 停止信号 FLASH: CS 高电平 */
	SPI_FLASH_CS_HIGH();
	
	/* 等待擦除完毕*/
	SPI_FLASH_WaitForWriteEnd();
}

/**
* @brief 对 FLASH 按页写入数据，调用本函数写入数据前需要先擦除扇区
* @param pBuffer，要写入数据的指针
* @param WriteAddr，写入地址
* @param NumByteToWrite，写入数据长度，必须小于等于页大小
* @retval 无
*
* 页大小是256个字节，就是一次写入数据不得超过256字节
*/
static void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	uint8_t *pBuffer_temp = pBuffer;
	
	/* 发送 FLASH 写使能命令 */
	SPI_FLASH_WriteEnable();

	/* 选择 FLASH: CS 低电平 */
	SPI_FLASH_CS_LOW();
	
	/* 发送写指令*/
	SPI_FLASH_Read_and_Write_Byte(W25X_PageProgram);
	
	/*发送写地址的高位*/
	SPI_FLASH_Read_and_Write_Byte((WriteAddr & 0xFF0000) >> 16);
	
	/*发送写地址的中位*/
	SPI_FLASH_Read_and_Write_Byte((WriteAddr & 0xFF00) >> 8);
	
	/*发送写地址的低位*/
	SPI_FLASH_Read_and_Write_Byte(WriteAddr & 0xFF);

	if (NumByteToWrite > SPI_FLASH_PerWritePageSize)
	{
		NumByteToWrite = SPI_FLASH_PerWritePageSize;
		FLASH_DEBUG_INFO("SPI flash page write too large!\n");
	}

	/* 写入数据*/
	while (NumByteToWrite--)
	{
		/* 发送当前要写入的字节数据 */
		SPI_FLASH_Read_and_Write_Byte(*pBuffer_temp);
		/* 指向下一字节数据 */
		pBuffer_temp++;
	}

	/* 停止信号 FLASH: CS 高电平 */
	SPI_FLASH_CS_HIGH();

	/* 等待写入完毕*/
	SPI_FLASH_WaitForWriteEnd();
}

uint8_t SPI_FLASH_Write(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	uint8_t *buffer_temp = pBuffer;
	uint8_t NumOfPage = 0;
	uint8_t NumOfSingle = 0;
	uint8_t ret = 0;
	
	if((buffer_temp != NULL) && (NumByteToWrite >= 1u))
	{
		NumOfPage = NumByteToWrite / 256u;
		NumOfSingle = NumByteToWrite % 256u;

		FLASH_DEBUG_INFO("spi_NumOfPage = %d, spi_NumOfSingle = %d\n", NumOfPage, NumOfSingle);
		
		while(NumOfPage--)
		{
			SPI_FLASH_PageWrite(buffer_temp, WriteAddr, SPI_FLASH_PerWritePageSize);

			/* 等待写入完毕*/
			SPI_FLASH_WaitForWriteEnd();

			WriteAddr += 256u;
			buffer_temp += 256u;
		}

		if(NumOfSingle >= 1u)
		{
			SPI_FLASH_PageWrite(buffer_temp, WriteAddr, NumOfSingle);
		}

	}
	else
	{
		FLASH_DEBUG_INFO("SPI input parameter error\n");
		
		ret = 0xFFu;
	}

	return ret;
}

void SPI_FLASH_Read(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
	/* 选择 FLASH: CS 低电平 */
	SPI_FLASH_CS_LOW();

	/* 发送 读 指令 */
	SPI_FLASH_Read_and_Write_Byte(W25X_ReadData);

	/* 发送 读 地址高位 */
	SPI_FLASH_Read_and_Write_Byte((ReadAddr & 0xFF0000) >> 16);
	/* 发送 读 地址中位 */
	SPI_FLASH_Read_and_Write_Byte((ReadAddr& 0xFF00) >> 8);
	/* 发送 读 地址低位 */
	SPI_FLASH_Read_and_Write_Byte(ReadAddr & 0xFF);

	/* 读取数据 */
	while (NumByteToRead--)
	{
		/* 读取一个字节*/
		*pBuffer = SPI_FLASH_Read_and_Write_Byte(DUMMY_BYTE);
		/* 指向下一个字节缓冲区 */
		pBuffer++;
	}

	/* 停止信号 FLASH: CS 高电平 */
	SPI_FLASH_CS_HIGH();
}

//唤醒
//当 SPI Flash 芯片处于睡眠模式时需要唤醒芯片才可以进行读写操作
void SPI_Flash_Wakeup(void)   
{
  /*选择 FLASH: CS 低 */
  SPI_FLASH_CS_LOW();

  /* 发送 上电 命令 */
  SPI_FLASH_Read_and_Write_Byte(W25X_ReleasePowerDown);

   /* 停止信号 FLASH: CS 高 */
  SPI_FLASH_CS_HIGH();
}  


