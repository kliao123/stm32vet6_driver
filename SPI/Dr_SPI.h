#ifndef __DR_SPI_H__
#define __DR_SPI_H__

#include "stm32f10x.h"
#include "stdio.h"
#include "Dr_SPI_parameter.h"



void FLASH_Init(void);
void SPI_Flash_Wakeup(void);
uint32_t SPI_Read_Flash_ID(void);
void SPI_FLASH_SectorErase(uint32_t SectorAddr);
uint8_t SPI_FLASH_Write(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_Read(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);

#endif

