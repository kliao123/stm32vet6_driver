#ifndef __DR_SPI_PARAMETER_H__
#define __DR_SPI_PARAMETER_H__

#include "DataType.h"

/*FLASH 常用命令*/
#define W25X_WriteEnable       0x06
#define W25X_WriteDisable      0x04
#define W25X_ReadStatusReg     0x05
#define W25X_WriteStatusReg    0x01
#define W25X_ReadData          0x03
#define W25X_FastReadData      0x0B
#define W25X_FastReadDual      0x3B
#define W25X_PageProgram       0x02
#define W25X_BlockErase        0xD8
#define W25X_SectorErase       0x20
#define W25X_ChipErase         0xC7
#define W25X_PowerDown         0xB9
#define W25X_ReleasePowerDown  0xAB
#define W25X_DeviceID          0xAB
#define W25X_ManufactDeviceID  0x90
#define W25X_JedecDeviceID     0x9F

/*flash 扇区地址*/
#define W25X_Block_0_Sector_0 ((uint32_t)0x000000u)
#define W25X_Block_0_Sector_1 ((uint32_t)0x001000u)
#define W25X_Block_0_Sector_2 ((uint32_t)0x002000u)
#define W25X_Block_0_Sector_3 ((uint32_t)0x003000u)
#define W25X_Block_0_Sector_4 ((uint32_t)0x004000u)
#define W25X_Block_0_Sector_5 ((uint32_t)0x005000u)

/*FLASH W25Q64 ID*/
#define FLASH_ID ((uint32_t)0xEF4017)

#endif

