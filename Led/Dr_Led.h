#ifndef DR_LED_H
#define DR_LED_H

#include "stm32f10x.h"
#include "stdio.h"


void Dr_Led_Init(void);

void Led_Red_On(void);
void Led_Red_Off(void);
void Led_Green_On(void);
void Led_Green_Off(void);
void Led_Blue_On(void);
void Led_Blue_Off(void);


#endif

