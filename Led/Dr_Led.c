#include "Dr_Led.h"


void Dr_Led_Init(void)
{
	GPIO_InitTypeDef GPIO_B_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	GPIO_B_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_B_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_B_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_B_InitStructure);

  //led off
	GPIO_SetBits(GPIOB, GPIO_Pin_5);
	GPIO_SetBits(GPIOB, GPIO_Pin_0);
	GPIO_SetBits(GPIOB, GPIO_Pin_1);
}

void Led_Red_On(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_5);
}

void Led_Red_Off(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_5);
}


void Led_Green_On(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_0);
}

void Led_Green_Off(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_0);
}

void Led_Blue_On(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_1);
}


void Led_Blue_Off(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_1);
}












