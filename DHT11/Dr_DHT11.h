#ifndef DR_DHT11_H
#define DR_DHT11_H

#include "stm32f10x.h"
#include "stdio.h"
#include "Dr_SysTick.h"


void Dr_DHT11_Init(void);
void Dr_DHT11_Read_Data(void);
uint8_t DHT11_Data_Available(void);
void DHT11_Get_Humidity(uint8_t *data_int, uint8_t *data_deci);
void DHT11_Get_Temperature(uint8_t *data_int, uint8_t *data_deci);


#endif

