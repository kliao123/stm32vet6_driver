#include "Dr_DHT11.h"

typedef struct
{
	uint8_t humi_int;    //湿度的整数部分
	uint8_t humi_deci;   //湿度的小数部分
	uint8_t temp_int;    //温度的整数部分
	uint8_t temp_deci;   //温度的小数部分
	uint8_t check_sum;   //校验和
	uint8_t available;   //数据有效性
}DHT11_Data_TypeDef;


#define GPIO_PIN_STATUS (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_7))

static DHT11_Data_TypeDef DHT11_Data;


void Dr_DHT11_Init(void)
{
	/*定义一个GPIO_InitTypeDef类型的结构体*/
	GPIO_InitTypeDef GPIO_InitStructure; 

	/*开启DHT11_Dout_GPIO_PORT的外设时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	

	/*选择要控制的DHT11_Dout_GPIO_PORT引脚*/															   
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;	

	/*设置引脚模式为通用推挽输出*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   

	/*设置引脚速率为50MHz */   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 

	/*调用库函数，初始化DHT11_Dout_GPIO_PORT*/
	GPIO_Init (GPIOC, &GPIO_InitStructure);

	DHT11_Data.humi_int = 0;    //湿度的整数部分
	DHT11_Data.humi_deci = 0;   //湿度的小数部分
	DHT11_Data.temp_int = 0;    //温度的整数部分
	DHT11_Data.temp_deci = 0;   //温度的小数部分
	DHT11_Data.check_sum = 0;   //校验和
	DHT11_Data.available = 0;

}


/* 使DHT11-DATA引脚变为推挽输出模式 */
static void DHT11_Mode_Output(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/*选择要控制的DHT11_Dout_GPIO_PORT引脚*/															   
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;	

	/*设置引脚模式为通用推挽输出*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   

	/*设置引脚速率为50MHz */   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	/*调用库函数，初始化DHT11_Dout_GPIO_PORT*/
	GPIO_Init(GPIOC, &GPIO_InitStructure);	 	 
}

/* 使DHT11-DATA引脚变为上拉输入模式 */
static void DHT11_Mode_Input(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/*选择要控制的DHT11_Dout_GPIO_PORT引脚*/	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;

	/*设置引脚模式为上拉输入模式*/ 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ; 

	/*调用库函数，初始化DHT11_Dout_GPIO_PORT*/
	GPIO_Init(GPIOC, &GPIO_InitStructure);	 
}


/* 
 * 从DHT11读取一个字节，MSB先行
 */
static uint8_t DHT11_ReadByte( void )
{
	uint8_t i, temp=0;

	for(i=0;i<8;i++)    
	{	 
		/*每bit以50us低电平标置开始，轮询直到从机发出 的50us 低电平 结束*/  
		while(GPIO_PIN_STATUS == Bit_RESET);

		/*DHT11 以26~28us的高电平表示“0”，以70us高电平表示“1”，
		*通过检测 x us后的电平即可区别这两个状 ，x 即下面的延时 
		*/
		SysTick_Delay_us(4); //延时 40us 这个延时需要大于数据0持续的时间即可	   	  

		if(GPIO_PIN_STATUS == Bit_SET)/* x us后仍为高电平表示数据“1” */
		{
			/* 等待数据1的高电平结束 */
			while(GPIO_PIN_STATUS == Bit_SET);

			temp|=(uint8_t)(0x01<<(7-i));  //把第7-i位置1，MSB先行 
		}
		else	 // x us后为低电平表示数据“0”
		{			   
			temp&=(uint8_t)~(0x01<<(7-i)); //把第7-i位置0，MSB先行
		}
	}

	return temp;
	
}

void Dr_DHT11_Read_Data(void)
{
	/*输出模式*/
	DHT11_Mode_Output();
	
	/*主机拉低*/
	GPIO_ResetBits(GPIOC, GPIO_Pin_7);
	
	/*延时18ms*/
	SysTick_Delay_us(1800);//18ms
	
	/*总线拉高 主机延时30us*/
	GPIO_SetBits(GPIOC, GPIO_Pin_7);
	SysTick_Delay_us(3);//30us
	
	/*主机设为输入 判断从机响应信号*/ 
	DHT11_Mode_Input();

	/*判断从机是否有低电平响应信号 如不响应则跳出，响应则向下运行*/
	if(Bit_RESET == GPIO_PIN_STATUS)
	{
		/*轮询直到从机发出 的80us 低电平 响应信号结束*/  
		while(Bit_RESET == GPIO_PIN_STATUS);

		/*轮询直到从机发出的 80us 高电平 标置信号结束*/
		while(Bit_SET == GPIO_PIN_STATUS);

		/*开始接收数据*/
		DHT11_Data.humi_int = DHT11_ReadByte();
		DHT11_Data.humi_deci = DHT11_ReadByte();
		DHT11_Data.temp_int = DHT11_ReadByte();
		DHT11_Data.temp_deci = DHT11_ReadByte();
		DHT11_Data.check_sum = DHT11_ReadByte();
		DHT11_Data.available = 0;

		/*读取结束，引脚改为输出模式*/
		DHT11_Mode_Output();
		/*主机拉高*/
		GPIO_SetBits(GPIOC, GPIO_Pin_7);

		if(DHT11_Data.check_sum == DHT11_Data.humi_int + DHT11_Data.humi_deci + DHT11_Data.temp_int+ DHT11_Data.temp_deci)
		{
			DHT11_Data.available = 1;
		}
	}
	else
	{
		printf("no respone signal\n");
	}

}

uint8_t DHT11_Data_Available(void)
{
	return DHT11_Data.available;
}

void DHT11_Get_Humidity(uint8_t *data_int, uint8_t *data_deci)
{
	*data_int = DHT11_Data.humi_int;
	*data_deci = DHT11_Data.humi_deci;
}

void DHT11_Get_Temperature(uint8_t *data_int, uint8_t *data_deci)
{
	*data_int = DHT11_Data.temp_int;
	*data_deci = DHT11_Data.temp_deci;
}



