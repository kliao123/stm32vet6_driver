#include "Dr_SWDL.h"


#define APP_STAGING_AREA_SIZE           ((uint16_t)0xC800)   //50k
#define INTERNAL_FLASH_SECTOR_SIZE      ((uint16_t)0x800)    //2k

#define APP_STAGING_AREA_ADDRESS_START  ((uint32_t)0x08012800)
#define APP_STAGING_AREA_ADDRESS_END    ((uint32_t)0x0801EFFF)

enum
{
	Download_Init,
	Download_Erase,
	Download_Write,
	Download_Complete

};

typedef struct
{
	uint16_t capp_date_byte_total; //通过串口获得总共多少字节数据
	uint16_t capp_date_byte_current; //当前处理了几个数据
}CAPP_DATE_BYTE_RECORD;

CAPP_DATE_BYTE_RECORD capp_record;

static uint8_t current_status = Download_Init;
static uint8_t FLASH_Erase_Status = 0;

static uint8_t capp_update_info_eeprom[5];

uint8_t Application_Download(uint16_t frame, uint8_t *data, uint8_t len)
{
	uint8_t EraseCounter = 0; //记录要擦除多少页
	static uint32_t Address = 0; //记录写入的地址
	uint32_t App_Data = 0; //记录写入的数据
	uint16_t NbrOfPage = 0; //记录写入多少页
	FLASH_Status FLASHStatus = FLASH_COMPLETE; //记录每次flash操作的结果
	uint8_t *data_temp = data;
	uint8_t data_index = 0;
	uint8_t ret = 0;
	uint16_t crc_result = 0;

	
	if(frame == 0)
	{
		current_status = Download_Erase;
		
		capp_record.capp_date_byte_total = (((uint16_t)data[0]) << 8) | ((uint16_t)data[1]);
	}
	else if(frame == 0xFFFFu)
	{
		current_status = Download_Complete;
	}

	switch(current_status)
	{
		case Download_Erase:
			if(FLASH_Erase_Status == 0)
			{
				/* 解锁 */
				FLASH_Unlock();
				
				/* 计算要擦除多少页 */
				NbrOfPage = APP_STAGING_AREA_SIZE / INTERNAL_FLASH_SECTOR_SIZE;

				/* 清空所有标志位 */
				FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);

				current_status = Download_Write;
				
				/* 按页擦除*/
				for (EraseCounter = 0; EraseCounter < NbrOfPage; EraseCounter++)
				{
					FLASHStatus = FLASH_ErasePage(APP_STAGING_AREA_ADDRESS_START + (INTERNAL_FLASH_SECTOR_SIZE * EraseCounter));
					if(FLASHStatus != FLASH_COMPLETE)
					{
						//printf("SWDL flash erase page error !!!\r\n");
						FLASH_Lock();
						return 1;
					}
				}

				FLASH_Erase_Status = 1;
				
				/* 向内部 FLASH 写入数据 */
				Address = APP_STAGING_AREA_ADDRESS_START;
				
				//printf("SWDL flash erase page successfully\r\n");
			}
			else
			{
				//printf("flash has been erased\r\n");
			}

			break;
			
		case Download_Write:
			while ((Address < APP_STAGING_AREA_ADDRESS_END) && (data_index < len))
			{
				App_Data = (uint32_t)((uint32_t)(data_temp[data_index]) | (uint32_t)(data_temp[data_index+1] << 8) | \
										(uint32_t)(data_temp[data_index+2] << 16) | (uint32_t)(data_temp[data_index+3] << 24));

				FLASHStatus = FLASH_ProgramWord(Address, App_Data);
				Address += 4;
				data_index += 4;

				capp_record.capp_date_byte_current += 4;

				if(FLASHStatus != FLASH_COMPLETE)
				{
					//printf("SWDL flash write error !!!\r\n");
					FLASH_Lock();
					return 1;
				}
			}
			//printf("SWDL flash write successfully\r\n");
			break;
			
		case Download_Complete:
			//printf("SWDL flash Complete\r\n");
			if(capp_record.capp_date_byte_current == capp_record.capp_date_byte_total)
			{
				capp_update_info_eeprom[0] = 0x01;
				capp_update_info_eeprom[1] = (uint8_t)(capp_record.capp_date_byte_total >> 8);
				capp_update_info_eeprom[2] = (uint8_t)(capp_record.capp_date_byte_total);

				crc_result = CRC16_CCITT(capp_update_info_eeprom, 3);
				
				capp_update_info_eeprom[3] = (uint8_t)(crc_result >> 8);
				capp_update_info_eeprom[4] = (uint8_t)(crc_result);

				I2C_EEPROM_Write(0x00, capp_update_info_eeprom, 5);
			}
			else
			{
				ret = 1;
			}
			
			FLASH_Lock();
			break;
			
		default:
			//printf("SWDL ERROR !!!\r\n");
			break;
	}

	return ret;
}




















