#include "Dr_TIM.h"


// 中断优先级配置
static void BASIC_TIM6_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	// 设置中断组为 0
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	// 设置中断来源
	NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn ;
	// 设置主优先级为 0
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	// 设置抢占优先级为 3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	// 使能中断
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_InitStructure);
}


/* Tim6 和 Tim7 是基本定时器 */
void BASIC_TIM6_Config(void)
{
#if 0

	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	// 开启定时器时钟,即内部时钟 CK_INT=72M
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

	// 累计 TIM_Period 个频率后产生一个更新或者中断
	// 时钟预分频数为 71，则驱动计数器的时钟 CK_CNT = CK_INT / (71+1)=1M
	TIM_TimeBaseStructure.TIM_Prescaler = 71;

	// 自动重装载寄存器的值(计数值)
	TIM_TimeBaseStructure.TIM_Period = 1000;

	// 计数器计数模式，基本定时器只能向上计数，没有计数模式的设置
	//TIM_TimeBaseStructure.TIM_CounterMode = ;

	// 时钟分频因子 ，基本定时器没有，不用管
	//TIM_TimeBaseStructure.TIM_ClockDivision = ;

	// 重复计数器的值，基本定时器没有，不用管
	//TIM_TimeBaseStructure.TIM_RepetitionCounter = ;

	// 初始化定时器
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);

	
	// 清除计数器中断标志位
	TIM_ClearFlag(TIM6, TIM_FLAG_Update);

	// Tim中断配置
	BASIC_TIM6_NVIC_Config();

	// 开启计数器中断
	TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);

	// 使能计数器
	TIM_Cmd(TIM6, ENABLE);

	// 暂时关闭定时器的时钟，等待使用
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, DISABLE);
#endif
}


























