#ifndef DR_KEY_H
#define DR_KEY_H

#include "stm32f10x.h"
#include "stdio.h"


void Dr_Key_Init(void);

uint8_t Key_1_Is_Pressed(void);
uint8_t Key_2_Is_Pressed(void);


#endif

