#include "Dr_Key.h"


void Dr_Key_Init(void)
{
	GPIO_InitTypeDef GPIO_A_InitStructure, GPIO_C_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE);

	//key 1
	GPIO_A_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_A_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOA, &GPIO_A_InitStructure);

  //key 2
	GPIO_C_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_C_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_C_InitStructure);
}


uint8_t Key_1_Is_Pressed(void)
{
	return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
}



uint8_t Key_2_Is_Pressed(void)
{
	return GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);
}









