#ifndef DR_DMA_H
#define DR_DMA_H

#include "stm32f10x.h"
//#include "stdio.h"



#define DMA_USART_Receive_Buffer_Length 256u


extern uint8_t DMA_USART_Buffer[6];

extern uint8_t DMA_USART_Receive_Buffer[DMA_USART_Receive_Buffer_Length];


void DMA_TX_Config(void);
void DMA_TX_Request(uint16_t Size);

void DMA_USART_Receive_Config(void);
void DMA_ResetBufferSize(void);


#endif

