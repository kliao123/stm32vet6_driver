#include "Dr_DMA.h"

#if 0
static void NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* 嵌套向量中断控制器组选择 */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	/* 配置 USART 为中断源 */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	/* 抢断优先级为 1 */
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	/* 子优先级为 1 */
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	/* 使能中断 */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	/* 初始化配置 NVIC */
	NVIC_Init(&NVIC_InitStructure);
}
#endif

uint8_t DMA_USART_Buffer[6] = {'e','m','p','t','y','\n'};


void DMA_TX_Config(void)
{
	DMA_InitTypeDef DMA_InitStructure;

	// 开启DMA时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// 外设地址
	DMA_InitStructure.DMA_PeripheralBaseAddr = USART1_BASE + 0x04; //串口数据寄存器地址
	// 存储器地址
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DMA_USART_Buffer;
	// 传输方向
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;//内存到外设
	// 传输数目
	DMA_InitStructure.DMA_BufferSize = 0;//后面发送时再具体设置
	// 外设地址增量模式
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //不增
	// 存储器地址增量模式
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 外设数据宽度
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 存储器数据宽度
	DMA_InitStructure.DMA_MemoryDataSize = DMA_PeripheralDataSize_Byte;
	// 模式选择
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal; //单次发送
	// 通道优先级
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	// 存储器到存储器模式
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel4, &DMA_InitStructure);
	DMA_Cmd(DMA1_Channel4, DISABLE);
}


uint8_t DMA_USART_Receive_Buffer[DMA_USART_Receive_Buffer_Length] = {0};

void DMA_USART_Receive_Config(void)
{
	DMA_InitTypeDef DMA_InitStructure;

	// 开启DMA时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// 外设地址
	DMA_InitStructure.DMA_PeripheralBaseAddr = USART1_BASE + 0x04; //串口数据寄存器地址
	// 存储器地址
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DMA_USART_Receive_Buffer;
	// 传输方向
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;//外设到内存
	// 传输数目
	DMA_InitStructure.DMA_BufferSize = DMA_USART_Receive_Buffer_Length;
	// 外设地址增量模式
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //不增
	// 存储器地址增量模式
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 外设数据宽度
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 存储器数据宽度
	DMA_InitStructure.DMA_MemoryDataSize = DMA_PeripheralDataSize_Byte;
	// 模式选择
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal; //单次发送
	// 通道优先级
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	// 存储器到存储器模式
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel5, &DMA_InitStructure);
	DMA_Cmd(DMA1_Channel5, ENABLE);
}


void DMA_ResetBufferSize(void)
{ 
	DMA_Cmd(DMA1_Channel5, DISABLE );  //关闭USART1 TX DMA1所指示的通道    
	DMA_SetCurrDataCounter(DMA1_Channel5, DMA_USART_Receive_Buffer_Length);
	DMA_Cmd(DMA1_Channel5, ENABLE);
} 



void DMA_TX_Request(uint16_t Size)
{
	static uint8_t first = 0;
	FlagStatus state = 0;

	//if(first == 0)
	//{
	//	state = SET;
	//	first = 1;
	//}
	//else
	//{
	//	state = DMA_GetFlagStatus(DMA1_FLAG_TC1);
	//}

	//if(state == SET)
	{
		//关闭DMA
		DMA_Cmd(DMA1_Channel4, DISABLE);

		//设置传输数目
		DMA_SetCurrDataCounter(DMA1_Channel4, Size);

		//打开DMA
		DMA_Cmd(DMA1_Channel4, ENABLE);
	}

	
}
















