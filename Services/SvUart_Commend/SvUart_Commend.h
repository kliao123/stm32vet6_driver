#ifndef SV_UART_COMMEND_H
#define SV_UART_COMMEND_H

#include "stm32f10x.h"
#include "Dr_DMA.h"
#include "Dr_SWDL.h"
#include "Dr_Uart.h"

#include "stdio.h"




enum
{
	Commend_Init,
	Commend_Reset,
	Commend_Download,
	Commend_count
};


typedef struct
{
	uint16_t successive;  //0:单帧数据, 1:连续数据第一帧, 2:连续数据第二帧......
	uint8_t commend_type; //指令
	uint8_t data_length;	//数据长度
	uint8_t data[252];
}Commend_Struct;




/*function*/
void Uart_Commend_Init(void);

void Uart_Commend_Processing(void);

void Set_Reveive_Commend_Flag(void);



#endif

