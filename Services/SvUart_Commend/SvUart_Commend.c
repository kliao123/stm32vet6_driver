#include "SvUart_Commend.h"


static uint8_t Reveive_Commend = 0;

static Commend_Struct *Commend_Data = (Commend_Struct *)DMA_USART_Receive_Buffer;



static void Uart_Commend_Reset(void)
{
	//复位前先关闭总中断
	__set_PRIMASK(1);

	//复位
	NVIC_SystemReset();
}



void Uart_Commend_Processing(void)
{
	uint8_t result = 0;
	
	if(Reveive_Commend == 1)
	{
		switch(Commend_Data->commend_type)
		{
			case Commend_Reset:
				Uart_Commend_Reset();
				break;
			
			case Commend_Download:
				Application_Download(Commend_Data->successive, Commend_Data->data, Commend_Data->data_length);
				break;
				
			default:
				printf("Commend Error\r\n");
				break;
		}

		Reveive_Commend = 0;

		if(result != 0)
		{
			DMA_USART_Receive_Buffer[4] = 0xFFu;
		}
		else
		{
			DMA_USART_Receive_Buffer[4] = DMA_USART_Receive_Buffer[3];
		}
		
		USART_Send_Data(DMA_USART_Receive_Buffer, 5u);
	}
}



void Uart_Commend_Init(void)
{
	Reveive_Commend = 0;
}




void Set_Reveive_Commend_Flag(void)
{
	Reveive_Commend = 1;
}


















