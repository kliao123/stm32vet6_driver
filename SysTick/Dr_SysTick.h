#ifndef DR_SYSTICK_H
#define DR_SYSTICK_H

#include "stm32f10x.h"
#include "stdio.h"

void SysTick_Init(void);
void SysTick_Delay_us(uint32_t Time); // Time * 10us
void SysTick_TimingDelay_Decrement(void);


#endif

