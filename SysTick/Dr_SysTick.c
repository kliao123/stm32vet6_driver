#include "Dr_SysTick.h"


static uint32_t SysTick_TimingDelay = 0;


void SysTick_Init(void)
{
	/* SystemFrequency / 1000 1ms 中断一次
	 * SystemFrequency / 100000 10us 中断一次
	 * SystemFrequency / 1000000 1us 中断一次
	 */

	if(SysTick_Config(SystemCoreClock / 100000))
	{
		/* Capture error */
		printf("system tick capture error!!!\n");
		while (1);
	}
}

void SysTick_Delay_us(uint32_t Time)
{
	SysTick_TimingDelay = Time;

	while (SysTick_TimingDelay != 0);
}



void SysTick_TimingDelay_Decrement(void)
{
	if(SysTick_TimingDelay != 0x00)
	{
		SysTick_TimingDelay--;
	}
}






