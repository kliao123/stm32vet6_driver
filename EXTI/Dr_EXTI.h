#ifndef DR_EXTI_H
#define DR_EXTI_H

#include "stm32f10x.h"
#include "stdio.h"

#define KEY1_INT_EXTI_IRQ EXTI0_IRQn      //PA0
#define KEY2_INT_EXTI_IRQ EXTI15_10_IRQn  //PC13

#define KEY1_INT_EXTI_LINE   EXTI_Line0
#define KEY2_INT_EXTI_LINE   EXTI_Line13

#define KEY1_INT_EXTI_PORTSOURCE GPIO_PortSourceGPIOA
#define KEY1_INT_EXTI_PINSOURCE  GPIO_PinSource0

#define KEY2_INT_EXTI_PORTSOURCE GPIO_PortSourceGPIOC
#define KEY2_INT_EXTI_PINSOURCE  GPIO_PinSource13



/* function */
void EXTI_Key_init(void);

#endif

