#include "Dr_ADC.h"

uint16_t ADC_ConvertedValue;

static void ADC_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	NVIC_InitStructure.NVIC_IRQChannel = ADC1_2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);
}

static void ADC_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	// 打开 ADC IO 端口时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

	// 配置 ADC IO 引脚模式
	// 必须为模拟输入
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;

	// 初始化 ADC IO
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}



static void ADC_Mode_Config(void)
{
	ADC_InitTypeDef ADC_InitStructure;
	
	// 打开ADC 时钟
	RCC_APB2PeriphClockCmd (RCC_APB2Periph_ADC1, ENABLE);

	// ADC 模式配置
	// 只使用一个ADC，属于独立模式
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	// 禁止扫描模式，多通道才要，单通道不需要
	ADC_InitStructure.ADC_ScanConvMode = DISABLE ;
	// 连续转换模式
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	// 不用外部触发转换，软件开启即可
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	// 转换结果右对齐
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	// 转换通道1 个
	ADC_InitStructure.ADC_NbrOfChannel = 1;

	// 初始化ADC
	ADC_Init(ADC1, &ADC_InitStructure);


	// 配置 ADC 时钟 8 分频，即9MHz, 最大不能超过14MHz
	RCC_ADCCLKConfig(RCC_PCLK2_Div8);

	// 配置 ADC 通道转换顺序为1，第一个转换，采样时间为55.5 个时钟周期
	ADC_RegularChannelConfig(ADC1, ADC_Channel_16, 1, ADC_SampleTime_55Cycles5);

	// ADC 转换结束产生中断，在中断服务程序中读取转换值
	ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);

	// 使能芯片内部温度传感器
	ADC_TempSensorVrefintCmd(ENABLE);

	// 开启ADC ，并开始转换
	ADC_Cmd(ADC1, ENABLE);

	// 初始化ADC 校准寄存器
	ADC_ResetCalibration(ADC1);
	
	// 等待校准寄存器初始化完成
	while (ADC_GetResetCalibrationStatus(ADC1));

	// ADC 开始校准
	ADC_StartCalibration(ADC1);
	
	// 等待校准完成
	while (ADC_GetCalibrationStatus(ADC1));

	// 由于没有采用外部触发，所以使用软件触发ADC 转换
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

}

void ADC_Init_Config(void)
{
	//ADC_GPIO_Config();
	ADC_NVIC_Config();
	ADC_Mode_Config();
}


void ADC_IRQHandler_GetValue(void)
{
	if(ADC_GetITStatus(ADC1,ADC_IT_EOC)==SET)
	{
		// 读取ADC 的转换值
		ADC_ConvertedValue = ADC_GetConversionValue(ADC1);

		ADC_ClearITPendingBit(ADC1,ADC_IT_EOC);
	}
}


uint16_t ADC_GetCPUTemperate(void)
{
	float Voltage = 0;
	uint16_t temperate = 0;
	
	Voltage = (float)((float)ADC_ConvertedValue * ( 3.3/ 4096.0)); //电压值 
	temperate = (uint16_t)(((1.43-Voltage)/0.0043+25) * 10);   //转换为温度值 

	return temperate;
}















