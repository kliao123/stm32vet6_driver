#ifndef DR_ADC_H
#define DR_ADC_H

#include "stm32f10x.h"
#include "stdio.h"







/* 初始化模块 */
void ADC_Init_Config(void);

/* 用于中断处理 */
void ADC_IRQHandler_GetValue(void);

/* 获取CPU温度 */
uint16_t ADC_GetCPUTemperate(void);








#endif

