#include "Dr_Uart.h"

#if 1
static void USART_NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* 嵌套向量中断控制器组选择 */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	/* 配置 USART 为中断源 */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	/* 抢断优先级为 1 */
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	/* 子优先级为 1 */
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	/* 使能中断 */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	/* 初始化配置 NVIC */
	NVIC_Init(&NVIC_InitStructure);
}
#endif

/* 使能了串口DMA接收中断 */
void USART_Init_Config(void)
{
	
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	//配置GPIO
	//配置前要先打开时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //TX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;//推挽复用模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入模式

	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//配置串口信息
	//配置前要先打开时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	USART_InitStructure.USART_BaudRate = 115200u;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;

	USART_Init(USART1, &USART_InitStructure);

	// 串口中断优先级配置
	USART_NVIC_Configuration();

	// 使能串口空闲中断
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);

	// 使能串口 DMA 接收
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);

	// 使能串口
	USART_Cmd(USART1, ENABLE);
}








void USART_Send_Data(uint8_t *pbuffer, uint8_t len)
{
	if((pbuffer != NULL) && (len > 0u))
	{
		while(len--)
		{
			/* 发送一个字节数据到串口 */
			USART_SendData(USART1, *pbuffer);

			/* 等待发送完毕 */
			while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);

			pbuffer++;
		}
	}
}




//重定向 c 库函数 printf 到串口，重定向后可使用 printf 函数
int fputc(int ch, FILE *f)
{
	/* 确认发送完毕 (这里是防止第一个字节丢失) */
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	
	/* 发送一个字节数据到串口 */
	USART_SendData(USART1, (uint8_t) ch);

	/* 确认发送完毕 (这里是防止bootloader跳转到app后前几个字节乱码) */
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

	return (ch);
}

///重定向 c 库函数 scanf 到串口，重写向后可使用 scanf、getchar 等函数
int fgetc(FILE *f)
{
	/* 等待串口输入数据 */
	while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
	
	return (int)USART_ReceiveData(USART1);
}

















