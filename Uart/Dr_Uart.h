#ifndef DR_USART_H
#define DR_USART_H

#include "stm32f10x.h"
#include "stdio.h"


void USART_Init_Config(void);
void USART_Send_Data(uint8_t *pbuffer, uint8_t len);

#endif

