#include "Dr_EEPROM.h"


/*通讯等待超时时间*/
#define I2C_CHECK_EVENT_TIMEOUT ((uint32_t)0x100000)

#define EEPROM_AT24C02_PAGE_SIZE (8u)

#define EEPROM_ADDRESS_WRITE 0xA0  //(0x50 << 1) | 0
#define EEPROM_ADDRESS_READ  0xA1  //(0x50 << 1) | 1

/* eeprom data */
uint8_t EEPROM_Data_SW_VERSION[3] = {0x00, 0x00, 0x00};


/* used to debug */
#define EEPROM_DEBUG 0

#if EEPROM_DEBUG
#define EEPROM_DEBUG_INFO printf
#else
#define EEPROM_DEBUG_INFO
#endif


static void I2C_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	/* I2C_SCL、I2C_SDA*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;  //SCL pin
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD; // 复用开漏输出,I2C的引脚必须使用这种模式
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	//GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7; //SDA pin
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	//GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD; // 复用开漏输出,I2C的引脚必须使用这种模式
	//GPIO_Init(GPIOB, &GPIO_InitStructure);
}

static void I2C_Config(void)
{
	I2C_InitTypeDef I2C_InitStruct;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

	I2C_InitStruct.I2C_ClockSpeed = 400000u;
	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStruct.I2C_OwnAddress1 = 0x0Fu; //0000 1111
	I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;

	I2C_Init(I2C1, &I2C_InitStruct);
	I2C_Cmd(I2C1, ENABLE);
}

void EEPROM_Init(void)
{
	/*设置超时等待时间*/
	uint32_t I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;

	I2C_GPIO_Config();
	I2C_Config();

#if 1
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY))
	{
		I2C1->CR1 |= 0x1<<15;  // SWRST 位置1
		
		if(!I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY))
		{
			I2C1->CR1 &= ~(0x1<<15);	 // SWRST 位清0;
			I2C_Config();
			break;
		}
		
		if ((I2CTimeout--) == 0) break;
	}
#endif
}

void I2C_EE_Busy_ReInit(void)
{
	/*设置超时等待时间*/
	uint32_t I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	// 拉高
	GPIO_SetBits(GPIOB, GPIO_Pin_6);
	GPIO_SetBits(GPIOB, GPIO_Pin_7);

	do
	{
		I2C1->CR1 |= 0x1<<15;  // SWRST 位置1

		if(!I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY))
		{
			I2C1->CR1 &= ~(0x1<<15);	 // SWRST 位清0;
			EEPROM_Init();
			EEPROM_DEBUG_INFO("eeprom re-init from busy status successfully 0x%X\n", I2CTimeout);
			break;
		}
		
		if((I2CTimeout--) == 0)
		{
			EEPROM_DEBUG_INFO("eeprom re-init from busy status failed\n");
			break;
		}
		
	}while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));

}


static uint8_t I2C_TIMEOUT_UserCallback(uint8_t errorCode)
{
	/* 使用串口 printf 输出错误信息，方便调试 */
	EEPROM_DEBUG_INFO("I2C timeout! errorCode = %d\n",errorCode);
	
	return errorCode;
}


static void I2C_EEPROM_WaitEepromStandbyState(void)
{
	vu16 SR1_Tmp = 0;

	do 
	{
		/* 发送起始信号 */
		I2C_GenerateSTART(I2C1, ENABLE);

		/* 读 I2C1 SR1 寄存器 */
		SR1_Tmp = I2C_ReadRegister(I2C1, I2C_Register_SR1);
		 
		/* 发送 EEPROM 地址 + 写方向 */
		I2C_Send7bitAddress(I2C1, EEPROM_ADDRESS_WRITE, I2C_Direction_Transmitter);
	}
	// SR1 位 1 ADDR：1 表示地址发送成功，0 表示地址发送没有结束
	// 等待地址发送成功
	while (!I2C_GetFlagStatus(I2C1, I2C_FLAG_ADDR));

	/* 清除 AF 位 */
	I2C_ClearFlag(I2C1, I2C_FLAG_AF);
	/* 发送停止信号 */
	I2C_GenerateSTOP(I2C1, ENABLE);
}


static uint8_t I2C_EEPROM_Write_page(uint8_t WriteAddr, uint8_t* pBuffer, uint8_t BufferLenth)
{
	/*设置超时等待时间*/
	uint32_t I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;

	// I2C bus is busy
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(1);
	}

	// I2C 起始信号
	I2C_GenerateSTART(I2C1, ENABLE);
	
  I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV5 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(2);
	}

	/* 发送 EEPROM 设备地址 */
	I2C_Send7bitAddress(I2C1, EEPROM_ADDRESS_WRITE, I2C_Direction_Transmitter);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV6 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(3);
	}

	/* 发送要写入的 EEPROM 内部地址(即 EEPROM 内部存储器的地址) */
	I2C_SendData(I2C1, WriteAddr);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV8 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(4);
	}

	/* 循环发送数据 */
	while(BufferLenth--)
	{
		/* 发送一字节要写入的数据 */
		I2C_SendData(I2C1, *pBuffer);
		
		//EEPROM_DEBUG_INFO("data length %d\n",BufferLenth);
			
		pBuffer++;

		I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
		/* 检测 EV8 事件并清除标志*/
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
		{
			if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(5);
		}
	}

		/* 发送停止信号 */
	I2C_GenerateSTOP(I2C1, ENABLE);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV8_2 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(6);
	}
	
	return 0;
}

uint8_t I2C_EEPROM_Write(uint8_t WriteAddr, uint8_t* pBuffer, uint8_t BufferLenth)
{
	uint8_t NumOfPage = 0;
	uint8_t NumOfSingle = 0;
	uint8_t *buffer_temp = pBuffer;
	uint8_t ret = 0;
	
	if((buffer_temp != NULL) && (BufferLenth >= 1u))
	{
		NumOfPage = BufferLenth / 8u;
		NumOfSingle = BufferLenth % 8u;

		EEPROM_DEBUG_INFO("NumOfPage = %d, NumOfSingle = %d\n", NumOfPage, NumOfSingle);
		
		while(NumOfPage--)
		{
			if(0 != I2C_EEPROM_Write_page(WriteAddr, buffer_temp, EEPROM_AT24C02_PAGE_SIZE))
			{
				EEPROM_DEBUG_INFO("write page state = %d\n", ret);
				break;
			}

			I2C_EEPROM_WaitEepromStandbyState();
			
			WriteAddr += 8u;
			buffer_temp += 8u;
		}

		if((NumOfSingle >= 1u) && (NumOfSingle < 8u) && (ret == 0))
		{
			ret = I2C_EEPROM_Write_page(WriteAddr, buffer_temp, NumOfSingle);

			EEPROM_DEBUG_INFO("write single state = %d\n", ret);
		}

	}
	else
	{
		EEPROM_DEBUG_INFO("I2C input parameter error\n");
		
		ret = 0xFFu;
	}

	return ret;
}

uint8_t I2C_EEPROM_Read(uint8_t ReadAddr, uint8_t* pBuffer, uint8_t BufferLenth)
{
	uint8_t* pBufferTemp = pBuffer;
	
	/*设置超时等待时间*/
	uint32_t I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;

	// I2C bus is busy
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(1);
	}

	// I2C 起始信号
	I2C_GenerateSTART(I2C1, ENABLE);
	
  I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV5 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(2);
	}

	/* 发送 EEPROM 设备地址 */
	I2C_Send7bitAddress(I2C1, EEPROM_ADDRESS_READ, I2C_Direction_Transmitter);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV6 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(3);
	}

	/* 发送要读取的 EEPROM 内部地址(即 EEPROM 内部存储器的地址) */
	I2C_SendData(I2C1, ReadAddr);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV8 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(4);
	}

	// 第二次发送 I2C 起始信号
	I2C_GenerateSTART(I2C1, ENABLE);
	
  I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV5 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(5);
	}

	/* 发送 EEPROM 设备地址 */
	I2C_Send7bitAddress(I2C1, EEPROM_ADDRESS_READ, I2C_Direction_Receiver);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV6 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(6);
	}

	/* 循环读取数据 */
	while(BufferLenth--)
	{
		I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
		/* 检测 EV7 事件, 即数据寄存器有新的有效数据*/
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED))
		{
			if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(7);
		}

		*pBufferTemp = I2C_ReceiveData(I2C1);

		pBufferTemp++;
	}

	/* 发送非应答信号 */
	I2C_AcknowledgeConfig(I2C1, DISABLE);

		/* 发送停止信号 */
	I2C_GenerateSTOP(I2C1, ENABLE);

	I2CTimeout = I2C_CHECK_EVENT_TIMEOUT;
	/* 检测 EV7 事件并清除标志*/
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED))
	{
		if ((I2CTimeout--) == 0) return I2C_TIMEOUT_UserCallback(8);
	}


	/* 使能应答，方便下一次 I2C 传输 */
	I2C_AcknowledgeConfig(I2C1, ENABLE);
	
	return 0;
}



