#ifndef __DR_EEPROM_H__
#define __DR_EEPROM_H__

#include "stm32f10x.h"
#include "stdio.h"


/* eeprom data */
extern uint8_t EEPROM_Data_SW_VERSION[3];


/* eeprom interface */
void EEPROM_Init(void);
void I2C_EE_Busy_ReInit(void);
uint8_t I2C_EEPROM_Write(uint8_t WriteAddr, uint8_t* pBuffer, uint8_t BufferLenth);
uint8_t I2C_EEPROM_Read(uint8_t ReadAddr, uint8_t* pBuffer, uint8_t BufferLenth);


#endif

